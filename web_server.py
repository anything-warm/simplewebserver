#!/usr/bin/env python3
"""
Very simple HTTP server in python
Usage:
    ./web_server.py -h
    ./web_server.py -l localhost -p 8000
Send a GET request:
    curl http://localhost:8000
Send a HEAD request:
    curl -I http://localhost:8000
Send a POST request:
    curl -d "foo=bar&bin=baz" http://localhost:8000
"""
import subprocess
import argparse
from http.server import HTTPServer, BaseHTTPRequestHandler


class S(BaseHTTPRequestHandler):
    def _set_headers(self):
        self.send_response(200)
        self.send_header("Content-type", "application/json")
        self.end_headers()

    def _html(self, message):
        """This just generates an HTML document that includes `message`
        in the body.
        """
        content = f"{message}"
        return content.encode("utf8")  # NOTE: must return a bytes object!

    def do_GET(self):
        temperature = subprocess.check_output(['sudo', './dht'])
        is_active = 'true' if subprocess.check_output(['gpio', 'read', '3']) == b'1\n' else 'false'
        message = "{temperature: %s, is_active: %s}" % (float(temperature), is_active)
        self._set_headers()
        self.wfile.write(self._html(message))

    def do_POST(self):
        # Turn on the heater here
        subprocess.check_output(['sudo', './enable_relay'])
        self._set_headers()

    def do_DELETE(self):
        # Turn off the heater here
        subprocess.check_output(['sudo', './disable_relay'])
        self._set_headers()


def run(server_class=HTTPServer, handler_class=S, addr="localhost", port=8000):
    server_address = (addr, port)
    httpd = server_class(server_address, handler_class)

    print(f"Starting httpd server on {addr}:{port}")
    httpd.serve_forever()


if __name__ == "__main__":

    parser = argparse.ArgumentParser(description="Run a simple HTTP server")
    parser.add_argument(
        "-l",
        "--listen",
        default="localhost",
        help="Specify the IP address on which the server listens",
    )
    parser.add_argument(
        "-p",
        "--port",
        type=int,
        default=8000,
        help="Specify the port on which the server listens",
    )
    args = parser.parse_args()
    run(addr=args.listen, port=args.port)

